"use strict";

let num = 0;

while (!Number.isInteger(num) || num == '' || num == null || isNaN(num)) {
    num = +prompt("Please enter an integer number", "");
}

if (num < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 5; i <= num; i+=5) {
        if(i % 5 == 0) {
            console.log(i);
        }
    }
}

